/**
 * main.js
 *
 * Bootstraps Vuetify and other plugins then mounts the App`
 */

// Components
import App from './App.vue'
import './assets/scss/style.scss'

// Composables
import { createApp } from 'vue'

// Plugins
import { registerPlugins } from '@/plugins'
import { registerComponents } from './helpers/register-component'

const app = createApp(App)

registerPlugins(app)
registerComponents(app)

app.mount('#app')
