import ButtonBase from "@/components/_commons/ButtonBase.vue";
import ButtonCatatan from "@/components/_commons/ButtonCatatan.vue";
import DividerVertical from "@/components/_commons/DividerVertical.vue";
import ProductCard from "@/components/_commons/ProductCard.vue"

export function registerComponents(app) {
  app
    .component("ButtonBase", ButtonBase)
    .component("ButtonCatatan", ButtonCatatan)
    .component("DividerVertical", DividerVertical)
    .component("ProductCard", ProductCard);
}
